section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rdi
    mov  rdx, rax
    mov  rsi, rdi
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xa
	call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov r8, 0xa
	mov rax, rdi
	dec rsp
	mov byte [rsp], 0
	mov rcx, 1
	.loop:
	    xor rdx, rdx
		div r8
		inc rcx
		add rdx, '0'
		dec rsp
		mov [rsp], dl
		cmp rax, 0
		jne .loop
	mov rdi, rsp
	push rcx
	call print_string
	pop rcx
	add rsp, rcx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi
    jnl .plus
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
	.plus:
		call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	push rbx
	push rdi
	push rsi
	call string_length
	pop rsi
	mov rdi, rsi
	push rsi
	push rax
	call string_length
	pop rdx
	pop rdi
	pop rsi
	cmp rax, rdx
	jne .error
	cmp rax, 0
	je .success
	xor rcx, rcx
	.loop:
		mov bl, [rdi + rcx]
		mov dl, [rsi + rcx]
		cmp bl, dl
 		jne .error
		inc rcx
		cmp rcx, rax
		je .success
		jmp .loop
	.success:
		mov rax, 1
		jmp .end
	.error:
		mov rax, 0
	.end:
		pop rbx
    	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    dec rsp
    mov rsi, rsp
    mov rdx, 1
    syscall
	cmp rax, 0
	je .result
	mov al, [rsp]
	.result:
		inc rsp
    	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

is_whitespace:
	cmp rdi, 0x20
	je .true
	cmp rdi, 0x9
	je .true
	cmp rdi, 0xa
	je .true
	.false:
		mov rax, 0
		ret
	.true:
		mov rax, 1
		ret

read_word:
	mov r9, 0 ; is reading word (1) or whitespace prefix (0)
	mov rdx, 1 ; 1 for null-terminator
	.loop:
		push rax
		push rcx
		push rdx
		push rsi
		push rdi
		call read_char
		mov r8, rax
		pop rdi
		pop rsi
		pop rdx
		pop rcx
		pop rax
		.check_eof:
			cmp r8, 0
			je .success
		.check_whitespace:
			push rdi
			mov rdi, r8
			call is_whitespace
			pop rdi
			cmp rax, 0
			je .is_whitespace_false
			.is_whitespace_true:
				cmp r9, 1
				je .success
				jmp .loop
			.is_whitespace_false:
				mov r9, 1
		.check_end_of_buffer:
			cmp rdx, rsi
			je .error
		mov [rdi + rdx - 1], r8 ; put symbol to buffer
		inc rdx
		jmp .loop
	.success:
		mov rax, rdi
		mov byte[rdi + rdx], 0 ; put null-terminator
		dec rdx
		jmp .end
	.error:
		mov rax, 0
	.end:
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    mov r8, 10
	.loop:
		cmp byte [rdi], '0'
		jb .end
		cmp byte [rdi], '9'
		ja .end
		push rdx
		mul r8
		pop rdx
		add al, byte [rdi]
		sub al, '0'
		inc rdx
		inc rdi
		jmp .loop
	.end:
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	mov al, [rdi]
	cmp al, '-'
	jne .plus
	.minus:
		inc rdi
		call parse_uint
		cmp rdx, 0
		je .end
		inc rdx
		neg rax
		ret
	.plus:
		call parse_uint
	.end:
		ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	; rdi - string pointer
	; rsi - buffer pointer
	; rdx - buffer length
	push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rdx, rax
	jl .error
	xor rcx, rcx
	.loop:
		cmp rcx, rdx
		je .error
		mov rax, [rdi]
		mov [rsi], rax
		cmp byte[rdi], 0
		je .end
		inc rcx
		inc rsi
		inc rdi
		jmp .loop
	.error:
		mov rax, 0
		ret
	.end:
		mov rax, rcx
		ret

